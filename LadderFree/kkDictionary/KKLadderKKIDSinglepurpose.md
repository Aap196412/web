###### 玩转金刚梯>金刚字典>

### 普通金刚号梯

- 一架[ 金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKID.md)中的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)如果以c9开头，则该[ 金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKID.md)为<strong> 普通金刚号梯 </strong>
- <strong> 普通金刚号梯 </strong>只可用于Windows+SSL客户端环境，不可用于其他环境

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)


