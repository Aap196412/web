###### 玩转金刚梯>金刚字典>
### 开源操作系统 OpenSourceOS
- 所谓<strong> 开源操作系统 </strong>是指源代码开放、任何人均可阅读的[ 操作系统 ](/LadderFree/kkDictionary/OS.md)
#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

