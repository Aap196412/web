###### 玩转金刚梯>金刚字典>

### 金刚流量包

[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)把[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)按不同[ 尺寸 ](/LadderFree/kkDictionary/KKDataTrafficPackageSize.md)分割为一个个包，再打上[ 有效期 ](/LadderFree/kkDictionary/KKDataTrafficPackageExpiretion.md)标志，形成月包、半年包、年包等流量包，便于售卖。


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

