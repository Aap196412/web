###### 金刚梯>金刚帮助>
### 金刚流量类
#### 术语与常识
- [金刚流量](/kkdatatraffic.md)
- [金刚流量包](/kkdatatrafficpackage.md)
- [金刚免费流量](/kkdatatrafficfree.md)
- [金刚1.0金刚号梯收费流量](/kkpriceofkkvpn1.0.md)
- [金刚2.0app梯收费流量](/kkpriceofkkvpn2.0.md)
- [金刚流量包尺寸](/kkdatatrafficsize.md)
- [金刚流量包有效期](/kkdatatrafficpackagevalidityperiod.md)
- [金刚流量包有效期的起算时刻](/kkdatatrafficpackagevalidityperiodstarttime.md)
- [流量提前耗尽](/kkdatatrafficisexhaustedearly.md)
- [流量过期](/kkdatatrafficexpired.md)
- [为何一定要区分<font color="Red"> 流量提前耗尽 </font>与<font color="Red"> 流量过期 </font>？](/distinguishingexpiration&exhaustion.md)
- [有人扫我散发的二维码并用短命邮箱注册成功，我因他注册成功所赚积分仍然有效，对吗？](/短命邮箱注册之奖励积分)
- [我刚买完流量，为何又收到续费提醒邮件？](/刚买流量又被提醒续费)
- [哪些情景属于 即买即用？](/哪些情景属于即买即用)
- [哪些情景属于 预购下一期流量？](/哪些情景属于预购下一期流量)
- [过期流量包内剩余流量还可以用吗？](/流量包过期后剩余流量还可以用吗)
- [大宗流量](/bulkkkdatatraffic.md)
- [小额流量](/smallamountkkdatatraffic.md)


#### 操作与实务
- [如何查询我的某个金刚号还是剩多少流量？](/howmanykkiddoihave.md)
- [我如何知道流量提前耗尽了？](/kkdatatrafficisexhaustedearlyidentify.md)
- [我如何知道流量过期了？](/kkdatatrafficexpiredidentify.md)
- [流量提前耗尽后我该怎么办？](/)
- [流量过期后我该怎么办？](/)
- [我想用积分买流量，该如何操作？](/thewaytobuydatatrafficwithpoints.md)

- [线下购买](/offlinepurchasedatatraffic.md)



[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()

[]()


[]()

[]()

[]()

[]()

[]()

#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
